# Les espaces aériens
**La consultation des restrictions aéronautiques reste sous la responsabilité de chaque pilote.**

## Lire une carte
La carte des espaces aériens français est disponible sur le site [Geoportail](https://www.geoportail.gouv.fr/carte) (onglet "Cartes">"Données thématiques">"Territoires et Transports">"Transports">"Carte OACI-VFR").

![Recherche Sup AIP](images/carte_oaci-vfr.png)

Il est aussi possible de les afficher sur :

- [Spotair](https://www.spotair.mobi/) (onglet "Couches de carte" > "Vol libre" > "Espaces aériens"). Il est possible de configurer les zones à afficher dans l'onglet "Préférences d'affichage" > "Espaces aérien".
- [Flyxc](https://flyxc.app/) ("Menu" > "Airspaces")

## Cartes AZBA / Réseau Très Basse Altitude (RTBA)
> Le RTBA est un ensemble de zones réglementées reliées entre elles et destiné aux vols d'entrainement à très basses altitude et à très grande vitesse des avions la Défense.
> Les vitesses des "chasseurs" qui évoluent dans le RTBA peuvent dépasser les 500KT (environ 900km/h).
> Les pilotes n'assurent pas la prévention des collisions.

Ces zones sont visibles sur la carte OACI-VFR (voir paragraphe Lire une carte) ou sur le site du [SIA au format PDF](https://www.sia.aviation-civile.gouv.fr/media/news/file//c/a/carte_rtba_edition_01_2023.pdf) ou [interactif qui affiche les activations en cours](https://azba.sia-france.fr/).

La vérification de l'activité des zones RTBA se fait sur le site du [SIA > Cartes AZBA](https://www.sia.aviation-civile.gouv.fr/schedules), attention  aux heures UTC!

Les zones nous concernant le plus directements sont la R 166 C pour les Monédières et la R 145 pour Guéret.

## NOTAM
Une méthode pour rechercher les zones actives sur un NOTAM est de simuler un vol passant proche du site, par exemple un vol allant de l'aéroport de Limoges à celui d'Egleton pour le site des Monédières (le site de Guéret n'a pas de zone concernée par le NOTAM).

Se rendre dans la partie [Préparation de vol](https://sofia-briefing.aviation-civile.gouv.fr/sofia/pages/prepavol.html) du site Sofia-Briefing puis "Navigation".

Compléter le formulaire suivant le lieu et la date recherchée, choisir "Trajet" dans le chapitre "NOTAM" :

![Formulaire NOTAM page 1](images/notam.png)

![Formulaire NOTAM page 2](images/notam2.png)

Cliquez ensuite sur le bouton "Rechercher" puis recherchez les zones aériennes susceptibles de vous intéressez grâce à la fonction rechercher dans la page de votre navigateur (touches "Ctrl" + "F").

Dans le cas du site des Monédières, les zones directements concernées sont les R68A, R68B, R68D et R368A.
La R368B commence 2.5km au sud du site de la Fournaise.

![Affichage NOTAM](images/notam_exemple.png)

- Q) Ligne de qualificateurs
- A) Lieux (ici entre Limoges et Egleton)
- B) Date de début d'activité de la zone
- C) Date de fin d'activité de la zone
- D) Créneaux horaire d'activité (UTC!)
- E) Zones concernées par l'activation
- F) Plancher d'activité de la zone
- G) Plafond d'activité de la zone

## Sup AIP
> Une ZRT ou Zone réglementée temporaire est une zone dans laquelle un aéronef ne peut circuler librement sans clairance spécifique délivrée par un service de contrôle civil ou militaire (Contact Radio obligatoire, Assignation de code transpondeur, d'altitude de transit...)

> Ces zones peuvent &ecirc;tre mises en place autour de zones à risques pendant des évènements nécessitant une régulation spécifique du trafic, ou autour de b&acirc;timents pour une durée temporaire.

Les ZRT sont déclarées dans les Sup AIP, disponibles sur le site du [SIA, "SUP AIP">"Métropole"](https://www.sia.aviation-civile.gouv.fr/documents/supaip/aip/id/6).

Recherchez par date les alertes qui semblent concerner les zones de vol, par exemple : "Création de trois zones réglementées temporaires (ZRT) dans la région Centre pour l'exercice MICA 17-06"

![Recherche Sup AIP](images/zrt.PNG)

Ensuite recherchez (F3 ou Ctrl+F) les zones aériennes impactant l'activité des sites, R 68 pour les monédières par exemple

![Recherche Sup AIP](images/zrt2.PNG)

## Zone Centre
Une zone centre constituée des zones R 68 A, B et C ainsi que des R 368 A et B est définie pour des exercices aériens durants plusieurs semaines.

Un [calendrier](https://www.sia.aviation-civile.gouv.fr/calendrier-zone-centre) est proposé sur le site de la SIA.
Ce calendrier permet d'avoir une estimation à plusieurs semaines des activations des zones mais ne se substitue pas à la vérification de l'activation des zones dans les NOTAM.

## Renseignements téléphonique
Un numéro vert du DIRCAM est disponible jusqu'à fin 2023 : 0800245466. Taper ensuite 1 puis le numéro de la zone à surveiller (ex : 68 pour les Monédières)

**Attention les ZRT ne sont pas renseignées par ce service**

La tour de contrôle de Limoges peut répondre aux demandes au 0555484030

## Abrévations / Conventions
Les heures sont exprimées en [UTC](http://www.worldtimeserver.com/heure-exacte-UTC.aspx), ajouter une heure l'hiver ou deux heures l'été pour obtenir l'heure française.

- SFC : Surface, utilisé pour indiquer qu'une zone commence au niveau du sol.
- ASFC : Above Surface, une altitude exprimée en fonction du niveau du sol.
- AMSL : Above Mean Sea Level, une altitude exprimée en fonction du niveau moyen de la mer.
- FLxxx : Altitude (100*xxx pour obtenir la valeur en pied)

[Autres abrévations >>>](http://federation.ffvl.fr/sites/ffvl.fr/files/Abreviations.pdf)

Conversion pieds/mètres : 1m = 3.28pieds

## Documentation
- [Page d'alerte de la FFVL](http://federation.ffvl.fr/pages/informations-vol-et-alertes-notam-et-sup-aip)
- [Aéroports proches de Limoges](http://ourairports.com/airports/LFBL/)
- [Document présentation réunion sécurité TVL 2008](http://www.unilim.fr/lec/vollibre/membres/ftp_membres/documents.php)
- [Guide de la consultation NOTAM](https://www.sia.aviation-civile.gouv.fr/media/news/file/g/u/guide_de_la_consultation_notam_-_13_juin_2022.pdf)
- [Présentation Sup AIP](https://www.sia.aviation-civile.gouv.fr/pub/media/news/file/n/o/notice_explicative_de_la_nouvelle_p_sentation_des_sup_aip.pdf)
- [B.A-Ba du RTBA](https://www.sia.aviation-civile.gouv.fr/pub/media/news/file/p/l/plaquettertba_version_web_pap.pdf)
- [Mode d'utilisation des cartes AZBA](https://www.sia.aviation-civile.gouv.fr/pub/media/news/file/m/o/mode_d_utilisation_azba_3.pdf)
- [NOTAM Info (anglais)](http://notaminfo.com/francemap)
