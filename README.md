# Ressources parapente
## Autres listes de ressources
- [Parapente360](https://parapente360.com) : Une collection de documents et vidéos sur différentes thématiques du parapente.
- [Tout le parapente](https://www.toutleparapente.fr/) (Inaccessible, définivement ?) : Une boutique en ligne proposant une collection de documents, liens et vidéos.

## Météorologie
### Prévisions
- [Windy](https://www.windy.com/) : Un site de prévisions météorologiques contenant plusieurs modèles. La lecture d'émagrammes est facilitée par l'utilisation du plugin [FlyXC Sounding](https://github.com/vicb/flyxc/blob/master/libs/windy-sounding/README.md).
- [Météoblue](https://www.meteoblue.com/fr) : Prévisions multimodèles.
- [Météo-parapente](https://meteo-parapente.com/) : Prévisions dédiées au vol libre, propose des émagrammes et lectures simplifiées des conditions.
- [Vélivole](https://www.velivole.fr/) : Autre site de prévisions dédiées au vol libre, émagrammes disponibles.
- [Soaringmeteo](https://soaringmeteo.org/GFSw/googleMap.html) : Site de prévisions dédiées au vol libre utilisant les modèle GFS et WRF.
- [Paraglidable](https://paraglidable.com/) : Site essayant d'estimer si les conditions de vols sont bonnes à partir d'une intelligence artificielle. Elle est entrainée en lui faisant comparer les prévisions précédentes avec les vols déclarés.
### Apprendre
- [Les Visiteurs du Ciel](https://www.flying-pages.com/shop_fr/product_info.php?products_id=2087) : Livre de référence d'Hubert Aupetit sur la météorologie appliquée au vol libre.
- [Cours météo](https://www.youtube.com/watch?v=stz9E4WQqqE)(youtube) : par Jean Oberson
- [Documentation Soaringmeteo](https://soaringmeteo.org/docs.html)
- [Compte rendu soirée outils météo TVL](https://gitlab.com/thermiquesvertslimousin/outils-meteo/-/raw/main/output/Presentation_outils_meteo.pdf)

## Balises, webcams
- [BaliseMeteo](http://www.balisemeteo.com/) : Le site de la FFVL pour l'affichage des mesures réalisées par les balises du réseau FFVL (uniquement).
- [Spotair](https://www.spotair.mobi/) : Un site affichant les mesures des balises FFVL, pioupiou plus les images de webcam disponibles.
- [OpenWindMap](https://www.openwindmap.org/) : Affichage des mesures du réseau pioupiou sur une carte.
- [winds.mobi](https://winds.mobi/stations/map) : Affichage des mesures de balises appartenants à différents réseaux à travers le monde.

## Sites de vol
- [Carte FFVL](https://carte.ffvl.fr/?mode=sites) : Une carte des sites officiels gérés par les clubs affiliés FFVL.
- [Paragliding map](https://www.paraglidingmap.com/app/) : Une carte répertoriant les sites de vol avec un affichage de la praticabilité des sites (basé sur la force et l'orientation du vent dans les prévisions). Affichage des emplacements probables des thermiques (basé sur les vols déclarés).
- [Paragliding Earth](http://www.paraglidingearth.com/) : Une autre carte affichant les sites de vol, semble être basé sur une base de données collaborative.
- [Spot Guru](https://www.spots.guru/) : Une carte tentant d'afficher les meilleurs sites de vol en fonction des prévisions météorologiques.

## Zones aériennes
- [SIA](https://www.sia.aviation-civile.gouv.fr/) : Le site à consulter avant de partir voler pour vérifier l'activation des zones aériennes.
- [Geoportail](https://www.geoportail.gouv.fr/carte) (fond Carte OACI-VFR, dans les catégories Territoire et transports > Transports) : Carte officielle des zones aériennes (hors ZRT)
- [OpenAIP](http://maps.openaip.net/) : Carte non officielle des zones aériennes.
- [Fly XC](https://flyxc.app/) (menu > activer `Airspaces`) : Carte "boite à outils" avec affichage des zones aériennes, des vols déclarés (source Thermal kk7), calcul de distance, calcul de points CFD.
- [Site de Pascal Bazile](http://pascal.bazile.free.fr/paraglidingFolder/divers/GPS/OpenAir-Format/) : Page de téléchargement des zones aériennes au format OpenAir.
- [poaff Airspace viewer](http://pascal.bazile.free.fr/poaff_airspaces/) : Visualisation des zones aériennes.
- [Notam Info](https://notaminfo.com/latest?country=France) : Affichage des NOTAM (non officiel)
- [HEHOL](https://hehol.fr/gliding) : Outil d'affichage des zones actives
- [Vérifier les zones actives dans le Limousin (TVL)](zones-aeriennes-limousin.md)

## Préparation cross
### Analyse des vols déclarés
- [FFVL - CFD](https://parapente.ffvl.fr/cfd) : Regroupe l'ensemble des vols déclarés par les pilotes à la fédération.
- Frontend [CFD - Parapente](https://cfd-parapente.com) pour afficher les vols déclarés sur la CFD sur le site FFVL.
- [XContest](https://www.xcontest.org/) : Une base de vols déclarés à travers le monde.
- [Thermal kk7](https://thermal.kk7.ch/) : Visualisation de l'ensemble des vols déclarés sur une carte. Indique les zones de départ en cross et les cheminements. Affiche aussi les zones thermiques.
- [Fly XC](https://flyxc.app/) : Carte "boite à outils" avec affichage des zones aériennes, des vols déclarés (source Thermal kk7), calcul de distance, calcul de points CFD.
- [XC Finder](https://xcfinder.com/) : Recherche de traces CFD en fonction de différents critères (lieu, date, distance, durée, ...)
- [XC-DB](https://xcdb.velivole.fr/) : Affichage de statistiques sur les vols CFD de type triangle par décollage. Propose une API pour utiliser ses données. [Code source du site](https://github.com/mmomtchev/xc-db) sous la licence GPL.

### Analyse de ses vols
- [XC Analytics](https://xcanalytics.fr/fr/) : Application Android (payante) proposant l'analyse des différentes phases d'un vol de distance afin d'améliorer ses performances.

### Tutoriels
- [Tuto pour les Jean Jo' - Tour du lac de Serre Ponçon](https://www.youtube.com/watch?v=X7axZdGi-gM) : Tour du lac de Serre Ponçon expliqué par Damien Lacaze (youtube).
- [Présentation du vol de groupe](https://ffvl-my.sharepoint.com/:v:/g/personal/j_garcia_ffvl_fr/ES8H79CyK_dDscV5jywI1IMBBtkQNZ6F-ae-MU3ngyQvpQ?e=0RfUTg) par Julien Garcia

### Divers
- [Paranche](https://www.parange.ch/) : Calcul des zones accessibles en partant d'un point en fonction du relief et de la finesse de la voile.
- Chapitre [Ressources dédiées à une région](#ressources-dédiées-à-une-région) de cette page

## Live tracking
- [LiveTrack24](https://www.livetrack24.com/) : Site d'affichage de live tracking, les applications permettant l'envoi de position sont notés sur la page [List of tracking Programs / Devices](https://www.livetrack24.com/apps/index).
- [Live tracking XContest](https://www.xcontest.org/api.s/widget/live-map/all/) : Affichage de live tracking, alimenté par l'application XCTrack.
- [SkyLines](https://skylines.aero/) : Site de déclaration de vols, permet l'affichage de live-tracking des pilotes équipés de SCSoar.
- [Live Flymaster](https://lt.flymaster.net/bs.php) : Affichage de live tracking, alimenté par les trackers Flymaster.
- [Live Sysride](https://www.syride.com/fr/live) : Affichage de live tracking, alimenté par les vario Syride.
- [Live Sport Track Live](https://www.sportstracklive.com/fr) : Affichage de live tracking, alimenté par l'application Sport Track Live.
- [Live Loctome](https://loctome.com/live) : Affichage de live tracking, alimenté par les applications Loctome, FlyShyHy, skyVario et le matériel Flymaster.
- [Puretrack](https://puretrack.io) : Affichage de live tracking, aggrège les données venant de différents systèmes de live tracking (Applications XCSoar, XCTrack, SeeYou Navigator, Flyskyhy, PureTrack, matériel SeeYou, Syride et Flymaster).
- [Outil VLSafe FFVL](https://intranet.ffvl.fr/mon_tracker) : Aggrégateur de live tracking, permet de mémoriser la dernière position du pilote pendant 1 mois pour améliorer la recherche en cas d'incident (la plupart des sites de live tracking n'affiche plus les positions quelques heures après la dernière émission). Pour l'instant, seuls des cadres FFVL ont accès aux positions.

## Matériel
- [Glider base](https://gliderbase.com/paragliders/) : Une base de données des voiles permettant de faire des recherches à partir de différents critères. Non exhaustif (notamment sur les voiles anciennes). Licence propriétaire.
- [para2000.p-h](https://para2000.p-h.click/wings/index.html) : Fork du site Para2000, aujourd'hui fermé. Une compilation d'informations techniques sur un très grand nombre de voiles. Licence inconnue.
- [Communauté de Vol Libre](https://cdv.li/bre/matos.html) (Inaccessible, définivement ?): Une autre base de données regroupant des informations sur les voiles. Licence CC-BY-NO-SA.
- [Parabible](https://www.paratroc.com/fr/content/45-parabible) : Base de données des voiles éditée par Paratroc. Licence inconnue.
- [Paratest](https://para-test.com/reports) : Base de données des rapports de certification de voile effectués par Air Turquoise
- [Argus Paratroc](https://www.paratroc.com/fr/content/17-calcul-cote-aile) : Une estimation du prix des voiles d'occasion. Édité par Paratroc.
- [Parafinder](https://www.parafinder.at) : Base de données de voiles, propose des stats à partir des vols déclarés sur XContest. Propose aussi des annonces d'accasion.
- [Dust of the Universe](https://ziadbassil.blogspot.com/) : Un blog d'essai de voiles
- [Page Informations et alertes sécurité de la FFVL](https://federation.ffvl.fr/pages/informations-et-alertes-s-curit) : Liste des alertes émises par la fédération et les fabricants
- [we-measure](https://we-measure.io) : Base de données collaborative de mesures des suspentages des voiles
- [Comparaglider](https://comparaglider.com/fr/) : Comparatifs de performances des différents modèles de voiles à partir de traces déclarées publiquement. A prendre avec précaution car les conditions de vols diffèrent (positionnement PTV, taille de la voile, conditions météorologiques, sellettes, utilisation de l'accélérateur, ...)

## Carnet de vol
- [Logfly](https://www.logfly.org/doku.php/doku.php) : Logiciel permettant de visualiser les vols et construire des statistiques à partir des traces des vols de l'utilisateur.
- [Carnet SeeYou](https://seeyou.cloud) : Site gratuit édité par le fabriquant Naviter.

## Instruments de vol
- [XCTrack](http://xctrack.org/) : Logiciel de navigation à installer sur smartphone (Android). Développé par l'équipe maintenant XContest. Permet le live tracking vers xcontest.org.
- [XCSoar](https://www.xcsoar.org/) : Logiciel de navigation à installer sur smartphone (Android), liseuse (kobo), windows ou linux. Open source (licence GNU GPL). Permet le live tracking vers LiveTrack24 et SkyLines.
- [LK8000](https://lk8000.it/) : Logiciel de navigation à installer sur smartphone (Android) ou liseuse (kobo). Open source (licence GNU GPL)
- [FlyMe](http://xcglobe.com/flyme/) : Application smartphone (Android) de navigation. Gratuit tant que la version finale n'est pas sortie. Licence propriétaire.
- [FlyShyHy](https://flyskyhy.com/) : Application smartphone (iOS) de navigation édité par Naviter. Demande de souscrire à un abonnement.
- [SeeYou Navigator](https://naviter.com/fr/seeyou-navigator/) : Application smartphone (Android et iOS) de navigation
- [GNUVario](https://prunkdump.github.io/GNUVario) : Vario open source (hardware + firmware)
- [GNUVario-E](https://prunkdump.github.io/GNUVario-TTGO-T5-website/) : Vario open source avec écran à encre électronique (hardware + firmware)

## Communautés de pilotes
- [Le Chant du Vario](http://www.parapentiste.info/forum/index.php) : Forum de volants français
- [Paragliding Forum](https://www.paraglidingforum.com/) : Forum de volants (en anglais)

## Technique
- [Rocher Bleu](https://rocherbleu.com/parapente/) : Quelques articles théoriques sur la mécaVol, le mental, des techniques de vol ou la météo
- Tuto pour les Jean Jo' - Voler Vite [Partie 1](https://www.youtube.com/watch?v=xiQRpa0-adE) et [Partie 2](https://www.youtube.com/watch?v=APzr2l7TVXY) : Vidéos youtube de Damien Lacaze sur le vol de performance.
- [Théorie de la performance](https://www.youtube.com/playlist?app=desktop&list=PLLtDX8ivpBj0wiDPLOnZrCsaMnJNigDpK&cbrd=1&ucbcb=1) : Série de vidéos youtube de Baptiste Lambert sur le vol de performance.
- [Pilotage Parapente](https://www.pilotage-parapente.com/) : Une formation vidéo sur les manoeuvres en parapente. Abonnement à l'année. Propose en accès libre l'analyse d'un [vrac de la semaine](https://www.pilotage-parapente.com/category/le-vrac-de-la-semaine/) survenu durant un stage SIV.
- [Tutoriels Théo de Blic](https://www.youtube.com/playlist?list=PLvdxFY-XKFBUNIjpr7Bjgc1qKaDbSuGkf) : Une chaîne youtube présentant des manoeuvres et figures acrobatiques.
- [Fly topo](https://www.youtube.com/playlist?list=PLxrzm0K0tnYNo-CfmTyNodXHWlFCEbUlB) : Une chaîne youtube proposant des exemples de vol bivouac par la marque Supair.

### Gonflage
- [Tutoriels "Control Técnico"](https://www.youtube.com/playlist?list=PLxrzm0K0tnYPQUwvGnxDLTCvVZ_ia4Pjz) : Une chaîne youtube présentant les manoeuvres au sol par Raul Rodriguez et la marque Supair.
- [Parapente - Manuel de pilotage au sol](https://www.youtube.com/playlist?list=PLYJefNpZhIBCIX9jpWiKO61wRoycH14lk) : Une chaîne youtube présentant différentes manoeuvres au sol par Cédric Guettet, un [document PDF](https://drive.google.com/file/d/1rpDYTTibSnFLYYzbCZLFrw7YbFE52q2_/view) explicatif est aussi disponible.
- [Ground handling challenge](https://andrebandarra.com/ghc) : Une liste d'exercices de gonflage sous forme de challenge, en anglais.

## Préparation Brevet Pilote (+ confirmé)
- [Préparation au QCM BP et BPC](https://qcm.ffvl.fr/)
- [Préparation aux questions ouvertes du BPC](https://sites.google.com/site/documentsparapentedelta/home/bpc-preparation-aux-questions-ouvertes-parapente-et-delta)

## Qualification biplace
- [Page d'information FFVL](https://parapente.ffvl.fr/qualification_biplace) : Contient entre autre le fascicule du biplaceur

## Développement
- [Open data FFVL](https://federation.ffvl.fr/pages/opendata) : Page contenant les données en open data de la FFVL (sites, balises)
- [igc-xc-score](https://github.com/mmomtchev/igc-xc-score) : Outil de calcul de score des vols à utiliser en ligne de commande ou à intégrer comme librairie javascript. Licence LGPL.
- [GPS dump](http://www.gpsdump.no/contents.htm) : Un programme d'extraction de traces depuis un grand nombre de GPS.

## Sécurité / accidents / incidents
- [Page Analyse accidents (FFVL)](https://federation.ffvl.fr/pages/analyses-accidents) : Page FFVL regroupant différents documents relatifs à l'accidentologie de la fédération
- [Compilation accidents mortels (FFVL)](https://docs.google.com/spreadsheets/d/1wxVhd0yYPexm0K1kTjykO-dWz9CoVrVmmgAS2jh-m3Y/edit#gid=2051165017) : Liste des accidents mortels de pilotes français (ainsi que de pilotes étrangers en France)
- [Page déclarer un accident (FFVL)](https://federation.ffvl.fr/pages/declarer_accident) : Page FFVL regroupant les informations sur la déclaration d'accident
- [Live des AS (Animateurs Sécurités)](https://www.youtube.com/playlist?list=PLceXUDmpcMR2G6i5KbFfBTz8T-e_-WUGu) : Une série de vidéos / podcast organisée par le groupe des Animateurs Sécurités de la FFVL.
- [Scoop.it Gestion des risques en vol libre](https://www.scoop.it/topic/securite-en-vol-libre) : Une collection d'articles, vidéos et autres ressources en lien avec la sécurité dans le monde du vol libre.

## Un peu d'histoire
- [Reportage sur le "vol de pente"](https://www.ina.fr/ina-eclaire-actu/video/caa7902072101/le-vol-de-pente) : Archive INA de 1979
- [Reportage 40 ans du vol libre au dessus du Puy de Dôme](https://www.youtube.com/watch?v=WjWWXjXdeDU) : Vidéo youtube retraçant l'histoire du delta au Puy de Dôme

## Ressources dédiées à une région
### Alpes
- [Carte des brises dans les Alpes](https://www.google.com/maps/d/viewer?mid=1LZ-3QtkG48alQEQfushGC6d_bAS9Y7o&ll=45.089344890460424%2C6.191574420381372&z=9)
- [Cross avancé - Club Saint-Hilaire](https://clubsthilair.fr/wp-content/uploads/2018/04/cross_massifs-et-transitions.pdf) : Document complet présentant les cheminements, transitions, brises et pièges des Aravis jusqu'au Dévoluy.
- [Passage de la Savoyarde](https://www.chvd.org/2020/06/22/le-passage-de-la-savoyarde/) : Billet de blog sur la transition du massif de la Chartreuse vers celui les Bauges.
- [Tuto pour les Jean Jo' - Tour du lac de Serre Ponçon](https://www.youtube.com/watch?v=X7axZdGi-gM) : Vidéo explicative de Damien Lacaze.

### Massif Central
- [Puy de Dôme parapente](https://pdd.dprslt.fr/) : Tableau de bord permettant de naviguer dans une aggrégation de données sur les sites du Puy de Dôme et alentours (balises, webcam, prévisions multi-sites, activation espaces aériens, horaires train Puy de Dôme, ...) 

## Divers
- [Voler Info](http://www.voler.info/) : Un magazine numérique.
- [Articles de presse - Soaring](https://www.soaring.fr/infos-utiles/articles-de-presse/) : Articles de presse écris par Marc Boyer.
- [replay.flights](https://replay.flights/) : Permet de revisionner ses vols en vue 3D
- [Site de la coupe du monde de parapente](https://pwca.org)
- [TT Task Creator](http://www.vololiberomontecucco.it/taskcreator/) : Préparer une manche de compétition
